from django.db import models
from django.utils import timezone


class Unit(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.TextField()
    deleted = models.BooleanField(default=False)
    timestamp = models.DateTimeField(default=timezone.now)
