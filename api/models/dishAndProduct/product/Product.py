from django.db import models
from django.utils import timezone
from api.models.dishAndProduct.product.Unit import Unit


class Product(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.TextField()
    validity = models.DateTimeField(default=timezone.now)
    storage = models.TextField()
    notes = models.TextField()
    unit = models.ForeignKey(Unit, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(default=timezone.now)
    counter = models.IntegerField(default=0)
    deleted = models.BooleanField(default=False)

