from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^products/limit/', views.get_products_limit, name='products_limit'),
    url(r'^products/', views.get_products, name='products'),
    url(r'^product/delete/', views.del_product, name='product_remove'),
    url(r'^product/add', views.add_product, name='product_add'),
    url(r'^product/edit', views.edit_product, name='product_edit'),
    url(r'^product/', views.get_product, name='product'),
    url(r'^units/', views.get_units, name='units'),
    url(r'^unit/add', views.add_unit, name='unit_add'),

]
