import json

from django.core.exceptions import ObjectDoesNotExist
from django.forms import model_to_dict
from django.http import HttpResponse
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt

from datetime import datetime

from api.models.dishAndProduct.product.Product import Product
from api.models.dishAndProduct.product.Unit import Unit

default_date = datetime.strptime('2000-01-01 00:00', '%Y-%m-%d %H:%M')

@csrf_exempt
def get_product(request):
    result = None
    try:
        pk = int(request.GET['ID'])
        if request.GET.get('timestamp'):
            timestamp = request.GET['timestamp']
        else:
            timestamp = default_date

        product = Product.objects.get(id=pk).filter(timestamp__gte=timestamp)
        result = json.dumps(model_to_json(product))
    except ObjectDoesNotExist:
        pass
    return HttpResponse(result, content_type="application/json")


@csrf_exempt
def get_products(request):
    result = None
    try:
        if request.GET.get('timestamp'):
            timestamp = request.GET['timestamp']
        else:
            timestamp = default_date

        newProducts = Product.objects.filter(deleted=False).filter(timestamp__gte=timestamp)
        deletedProducts = Product.objects.filter(deleted=True).filter(timestamp__gte=timestamp)
        result = {'newData': list_to_json(newProducts), 'removeData': list_to_json(deletedProducts)}
        result = json.dumps(result)
    except ObjectDoesNotExist:
        pass
    return HttpResponse(result, content_type="application/json")


def get_products_limit(request):
    result = None
    try:
        if request.GET.get('timestamp'):
            timestamp = request.GET['timestamp']
        else:
            timestamp = default_date
        limit = int(request.GET['limit'])
        offset = int(request.GET['offset'])

        newProducts = Product.objects.filter(deleted=False).filter(timestamp__gte=timestamp)[offset:offset+limit]
        deletedProducts = Product.objects.filter(deleted=True).filter(timestamp__gte=timestamp)
        result = {'newData': list_to_json(newProducts), 'removeData': list_to_json(deletedProducts)}
        result = json.dumps(result)
    except ObjectDoesNotExist:
        pass
    return HttpResponse(result, content_type="application/json")


@csrf_exempt
def del_product(request):
    try:
        pk = int(request.GET['ID'])

        product = Product.objects.get(id=pk)
        product.deleted = True
        product.save()
    except ObjectDoesNotExist:
        pass
    return HttpResponse({}, content_type="application/json")


@csrf_exempt
def add_product(request):
    result = None
    try:
        json_data = json.loads(request.body.decode('utf-8'))
        name = json_data['name']
        validity = json_data['validity']
        storage = json_data['storage']
        notes = json_data['notes']
        unit_json = json_data['unit']

        unit_id = unit_json['id']
        unit = Unit.objects.get(id=unit_id)

        product = Product(name=name, validity=validity, storage=storage, notes=notes, unit=unit)
        product.save()
        result = json.dumps(model_to_json(product))
    except ObjectDoesNotExist:
        pass
    return HttpResponse(result, content_type="application/json")


@csrf_exempt
def edit_product(request):
    result = None
    try:
        json_data = json.loads(request.body.decode('utf-8'))
        pk = int(json_data['id'])
        name = json_data['name']
        validity = json_data['validity']
        storage = json_data['storage']
        notes = json_data['notes']
        timestamp = timezone.now
        unit_json = json_data['unit']

        unit_id = unit_json['id']
        unit = Unit.objects.get(id=unit_id)

        product = Product.objects.get(id=pk)
        product.name = name
        product.validity = validity
        product.storage = storage
        product.notes = notes
        product.unit = unit
        product.timestamp = timestamp
        product.save()
        result = json.dumps(model_to_json(product))
    except ObjectDoesNotExist:
        pass
    return HttpResponse(result, content_type="application/json")


@csrf_exempt
def get_units(request):
    result = None
    try:
        if request.GET.get('timestamp'):
            timestamp = request.GET['timestamp']
        else:
            timestamp = default_date
        units = Unit.objects.all().filter(timestamp__gte=timestamp)
        result = json.dumps(list_to_json(units))
    except ObjectDoesNotExist:
        pass
    return HttpResponse(result, content_type="application/json")


@csrf_exempt
def add_unit(request):
    result = None
    try:
        json_data = json.loads(request.body.decode('utf-8'))
        name = json_data['name']

        unit = Unit(name=name)
        unit.save()
        result = json.dumps(model_to_json(unit))
    except ObjectDoesNotExist:
        pass
    return HttpResponse(result, content_type="application/json")


def list_to_json(objects):
    data = []
    for z in objects:
        data.append(model_to_json(z))
    return data


def model_to_json(z):
    temp = {}
    for x in model_to_dict(z):
        if str_value(getattr(z, x, '')) != (None and ""):
            temp[x] = str_value(getattr(z, x, ''))
    return temp


def str_value(x):
    if isinstance(x, (int, float)):
        return x
    elif isinstance(x, (type(None))):
        return None
    elif isinstance(x, Unit):
        return model_to_json(x)
    return str(x)
